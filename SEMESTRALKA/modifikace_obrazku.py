#!/usr/bin/env python3

import sys
import os
import numpy as np
from PIL import Image as im


####################################################
#modifikacni funkce

#zmenseni obrazku
#odebrani kazdeho x-teho pixelu
def zmenseni():
	print("")
	for i in range(sirka):
		print("=",end="")
	print("\n")
	print("Vybrali jste moznost zmenseni obrazku!\n\nZmenseni obrazku je v teto aplikaci zarizeno pomoci vybrani pouze urcite podmnoziny vsech pixelu.\nChcete-li napriklad zmensit obrazek na polovinu, vybere se pouze kazdy druhy pixel puvodniho obrazku.\n\nAplikace Vam umoznuje zvolit si, kolikrat chcete puvodni obrazek zmensit (2x zmensi obrazek na polovinu, 3x na tretinu, atd.).\n\nMejte vsak na pameti, ze s velikosti se ztraci i presnost rozliseni, a proto je moznost zmenseni omezena pouze do 10ti nasobneho zmenseni (vetsi zmenseni uz nema smysl delat, nebot by se ztratilo prilis mnoho informace).\n")
	for i in range(sirka):
		print("=",end="")
	print("\n")
	pomer = input('Zadejte, kolikrat chcete obrazek zmensit (prirozene cislo)!\n')
	try:
		pom=int(pomer)
		if pom<1 or pom>10:
			raise ValueError
	except ValueError:
		print('\nCHYBA: Nezadali jste cislo od 1 do 10!')
		sys.exit()
	else:
		mensi=obr[::pom,::pom,::]
	print("")
	for i in range(sirka):
		print("=",end="")
	print("\n")
	print("HOTOVO!\nVysledny obrazek '{}_mensi.{}' je {}x mensi nez puvodni obrazek '{}'.".format(obrjm,form,pom,obrsoubor))
	print("")
	for i in range(sirka):
		print("=",end="")
	print("")
	return mensi


#otoceni obrazku
#otaceni probiha pomoci transpozice matice a otoceni radku
def otoceni():
	print("")
	for i in range(sirka):
		print("=",end="")
	print("\n")
	print("Vybrali jste moznost otoceni obrazku!\n\nAplikace umoznuje otocit obrazek o 90 stupnu doleva (-90) nebo doprava (+90) anebo o 180 stupnu.\n")
	for i in range(sirka):
		print("=",end="")
	print("\n")
	stupne = input('Zadejte pocet stupnu o kolik chcete obrazek otocit (-90/90/180)!\n')
	try:
		st=int(stupne)
		if not (st==-90 or st==90 or st==180):
			raise ValueError
	except ValueError:
		print('\nCHYBA: Nezadali jste spravny pocet stupnu! Moznosti josu: -90, 90 a 180.')
		sys.exit()
	else:
		if st!=-90:
			otocene=np.transpose(obr[::-1,::,::],axes=[1,0,2])
			pocet=90
			smer=" doleva"
			if st==180:
				otocene=np.transpose(otocene[::-1,::,::],axes=[1,0,2])
				pocet=180
				smer=""
		else:
			ot=np.transpose(obr,axes=[1,0,2])
			otocene=ot[::-1,::,::]
			pocet=90
			smer=" doprava"
	print("")
	for i in range(sirka):
		print("=",end="")
	print("\n")
	print("HOTOVO!\nVysledny obrazek '{}_otoceny.{}' je otoceny o {} stupnu{} od puvodniho obrazku '{}'.".format(obrjm,form,pocet,smer,obrsoubor))
	print("")
	for i in range(sirka):
		print("=",end="")
	print("")
	return otocene


#prevraceni obrazku
#prevraceni se dosahne otocenim rakdu nebo sloupcu
def prevraceni():
	print("")
	for i in range(sirka):
		print("=",end="")
	print("\n")
	print("Vybrali jste moznost prevraceni obrazku!\n\nAplikace umoznuje prevratit obrazek vertikalne (v) nebo horizontalne (h).\n")
	for i in range(sirka):
		print("=",end="")
	print("\n")
	kam = input('Zadejte jak chcete obrazek prevratit (v/h)!\n')
	try:
		if not (kam=='v' or kam=="V" or kam=='h' or kam=='H'):
			raise ValueError
	except ValueError:
		print('\nCHYBA: Nezadali jste spravne pismenko prevraceni!')
		sys.exit()
	else:
		if kam=='v' or kam=="V":
			prevracene=obr[::-1,::,::]
			smer="vertikalne"
		if kam=='h' or kam=='H':
			prevracene=obr[::,::-1,::]
			smer="horizontalne"
	print("")
	for i in range(sirka):
		print("=",end="")
	print("\n")
	print("HOTOVO!\nVysledny obrazek '{}_prevraceny.{}' je {} prevraceny oproti puvodnimu obrazku '{}'.".format(obrjm,form,smer,obrsoubor))
	print("")
	for i in range(sirka):
		print("=",end="")
	print("")
	return prevracene


#inverzni obrazek
#pravod pomoci vektoroveho odectu na negativ
def inverze():
	print("")
	for i in range(sirka):
		print("=",end="")
	print("\n")
	print("Vybrali jste moznost inverzniho obrazku!\n\nObrazek bude preveden na negativni verzi puvodniho obrazku.\n")
	negativ=255-obr
	for i in range(sirka):
		print("=",end="")
	print("\n")
	print("HOTOVO!\nVysledny obrazek '{}_inverzni.{}' je vyobrazen v inverznich barvach puvodniho obrazku '{}'.".format(obrjm,form,obrsoubor))
	print("")
	for i in range(sirka):
		print("=",end="")
	print("")
	return negativ



#prevod obrazku do odstiu sedi
#pouzita rovnice pro prevod z RGB z 10. cviceni BI-PYT
#Y′601=0.299*R′+0.587*G′+0.114*B′
def seda():
	print("")
	for i in range(sirka):
		print("=",end="")
	print("\n")
	print("Vybrali jste moznost prevodu obrazku do odstinu sedi!\n\nTo je v aplikaci zarizeno tak, ze pro kazdy pixel se hodnoty RGB slozek zprumeruji podle rovnice 'Y=0.299*R+0.587*G+0.114*B'.\n\nKazdy pixel se prepocitava zvlast, proto muze tato modifikace trvat dele.\n")
	for i in range(sirka):
		print("=",end="")
	print("\n")
	print("PRACUJI ...")
	sedy=obr.copy()
	for rad in (range(obr.shape[0])):
		for sl in range(obr.shape[1]):
			seda=round(0.299*obr[rad][sl][0]+0.587*obr[rad][sl][1]+0.114*obr[rad][sl][2])
			sedy[rad][sl][0]=seda
			sedy[rad][sl][1]=seda
			sedy[rad][sl][2]=seda
	print("")
	for i in range(sirka):
		print("=",end="")
	print("\n")
	print("HOTOVO!\nVysledny obrazek '{}_sedy.{}' je vyobrazen pouze v odstinech sedi z puvodniho obrazku '{}'.".format(obrjm,form,obrsoubor))
	print("")
	for i in range(sirka):
		print("=",end="")
	print("")
	return sedy



#zesvetleni obrazku
#vynasobeni vsech barevnych slozek danym cislem (s horni hranici 255)
def zesvetleni():
	print("")
	for i in range(sirka):
		print("=",end="")
	print("\n")
	print("Vybrali jste moznost zesvetleni obrazku!\n\nZesvetleni obrazku je v teto aplikaci zarizeno pomoci zvyseni hodnot vsech barevnych slozek vsech pixelu.\n\nAplikace Vam umoznuje zvolit si, kolikrat chcete puvodni obrazek zesvetlit (tolikrat se zvetsi hodnoty barevnych slozek).\n\nMejte vsak na pameti, ze pokud bude vysledne cislo slozky vetsi nez 255, zustane pixel na teto hodnote a dal se zvysovat nebude, a proto je moznost zesvetleni omezena pouze do 10ti nasobneho zesvetleni (vetsi zesvetleni uz nema smysl delat, nebot by vetsina obrazku zustala pouze bila).\n")
	for i in range(sirka):
		print("=",end="")
	print("\n")
	pomer = input('Zadejte, kolikrat chcete obrazek zesvetlit (prirozene cislo)!\n')
	try:
		pom=int(pomer)
		if pom<1 or pom>10:
			raise ValueError
	except ValueError:
		print('\nCHYBA: Nezadali jste cislo od 1 do 10!')
		sys.exit()
	else:
		svetly=obr*pom
		idx=(obr>255//pom)
		svetly[idx]=255
	print("")
	for i in range(sirka):
		print("=",end="")
	print("\n")
	print("HOTOVO!\nVysledny obrazek '{}_sevtly.{}' je {}x svetlejsi nez puvodni obrazek '{}'.".format(obrjm,form,pom,obrsoubor))
	print("")
	for i in range(sirka):
		print("=",end="")
	print("")
	return svetly
	



#ztmaveni obrazku
#vydeleni vsech barevnych slozek danym cislem
def ztmaveni():
	print("")
	for i in range(sirka):
		print("=",end="")
	print("\n")
	print("Vybrali jste moznost ztmaveni obrazku!\n\nZtmaveni obrazku je v teto aplikaci zarizeno pomoci snizeni hodnot vsech barevnych slozek vsech pixelu.\n\nAplikace Vam umoznuje zvolit si, kolikrat chcete puvodni obrazek ztmavit (tolikrat se zmensi hodnoty barevnych slozek).\n\nMejte vsak na pameti, ze hodnoty lze snizovat pouze do 0, a proto je moznost ztmaveni omezena pouze do 100 nasobneho ztmaveni (vetsi ztmaveni uz nema smysl delat, nebot by vetsina obrazku zustala pouze cerna).\n")
	for i in range(sirka):
		print("=",end="")
	print("\n")
	pomer = input('Zadejte, kolikrat chcete obrazek ztmavit (prirozene cislo)!\n')
	try:
		pom=int(pomer)
		if pom<1 or pom>100:
			raise ValueError
	except ValueError:
		print('\nCHYBA:Nezadali jste cislo od 1 do 100!')
		sys.exit()
	else:
		tmavy=obr//pom
	print("")
	for i in range(sirka):
		print("=",end="")
	print("\n")
	print("HOTOVO!\nVysledny obrazek '{}_tmavy.{}' je {}x tmavsi nez puvodni obrazek '{}'.".format(obrjm,form,pom,obrsoubor))
	print("")
	for i in range(sirka):
		print("=",end="")
	print("")
	return tmavy


#zvyrazneni hran v obrazku
def zvyrazneni():
	print("")
	for i in range(sirka):
		print("=",end="")
	print("\n")
	print("Vybrali jste moznost zvyrazneni hran obrazku!\n\nTo je v aplikaci zarizeno tak, ze pro kazdou barevnou slozku v kazdem pixelu se vyberou jeji susedi v matici a jejich soucet se odecte od devitinasobku hodnoty dane slozky.\n\nKazda slozka vsech pixelu se prepocitava zvlast, proto muze tato modifikace trvat dele.\n")
	for i in range(sirka):
		print("=",end="")
	print("\n")
	print("PRACUJI ...")
	zvyrazneny=obr.copy()
	for rad in (range(1,obr.shape[0]-1)):
		for sl in range(1,obr.shape[1]-1):
			for bar in range(obr.shape[2]):
				vys=( 9*obr[rad][sl][bar] - obr[rad-1][sl][bar] - obr[rad+1][sl][bar] - obr[rad][sl-1][bar] - obr[rad][sl+1][bar] - obr[rad-1][sl-1][bar] - obr[rad-1][sl+1][bar] - obr[rad+1][sl-1][bar] - obr[rad+1][sl+1][bar] )
				if vys<0:
					vys=0
				if vys>255:
					vys=255
				zvyrazneny[rad][sl][bar]=vys
	print("")
	for i in range(sirka):
		print("=",end="")
	print("\n")
	print("HOTOVO!\nVysledny obrazek '{}_zvyrazneny.{}' je vyobrazen ostreji nez puvodni obrazek '{}'.".format(obrjm,form,obrsoubor))
	print("")
	for i in range(sirka):
		print("=",end="")
	print("")
	return zvyrazneny


####################################################

#spusteni skriptu
if len(sys.argv) == 2:
	if sys.argv[1]=='-h' or sys.argv[1]=='--help':
		print("\nUSAGE:\t",sys.argv[0],"\n\t",sys.argv[0],"-h/--help\n\nAplikaci pro modifikaci obrazku spustite jednoduse zavolanim skriptu './modifikace_obrazku.py' z prikazove radky! \nAplikace pracuje interaktivne.\n\nNejprve budete muset zadat vstupni obrazek a pote si vybrat jakou operaci s nim budete chtit provest a s jakymi parametry.\n\nImplementovane modifikace jsou:\n\t1) zmenseni obrazku\n\t2) otoceni obrazku\n\t3) prevraceni obrazku\n\t4) inverzni obrazek\n\t5) prevod do odstinu sedi\n\t6) zesvetleni\n\t7) ztmaveni\n\t8) zvyrazneni hran\n\nVysledny obrazek se ulozi pod nazvem puvodniho obrazku s priponou konkretni provedene modifikace.\n")
		sys.exit()
	else:
		print("\nUSAGE:\t",sys.argv[0],"\n\t",sys.argv[0],"-h/--help\n")
		sys.exit()
else:
	if len(sys.argv) != 1:
		print("\nUSAGE:\t",sys.argv[0],"\n\t",sys.argv[0],"-h/--help\n")
		sys.exit()


#nastaveni vstupu a modifikace
sirka, vyska = os.get_terminal_size(0)
for i in range(sirka):
	print("=",end="")
print("\n")
print("Vitejte v aplikaci pro modifikaci JPEG obrazku!\n\nAplikace pracuje interaktivne.\n\nNejprve budete muset zadat vstupni obrazek a pote si vybrat jakou operaci s nim budete chtit provest a s jakymi parametry.\n\nVysledny obrazek se ulozi pod nazvem puvodniho obrazku s priponou konkretni provedene modifikace.\n")
for i in range(sirka):
	print("=",end="")
print("\n")
#vstupni obrazek
obrsoubor = input("Zadejte cestu ke vstupnimu obrazku!\n")
try:
	if os.path.exists(obrsoubor):
		if not os.path.isfile(obrsoubor):
			raise FileExistsError
	else:
		raise FileExistsError
	form=obrsoubor.split('.')[-1]
	if form!='jpg' and form!='JPG':
		raise TypeError
except FileExistsError:
	print("\nCHYBA: Soubor '{}' neexistuje.".format(obrsoubor))
except TypeError:
	print("\ncHYBA: Soubor '{}' neni v pozadovanem formatu JPEG.".format(obrsoubor))
else:
	#pozadovana modifikace
	print("")
	for i in range(sirka):
		print("=",end="")
	print("\n")
	print("Nyni si muzete vybrat modifikaci, kterou si prejete na obrazek aplikovat.\nImplementovane modifikace jsou:\n\t1) zmenseni obrazku\n\t2) otoceni obrazku\n\t3) prevraceni obrazku\n\t4) inverzni obrazek\n\t5) prevod do odstinu sedi\n\t6) zesvetleni\n\t7) ztmaveni\n\t8) zvyrazneni hran\n")
	for i in range(sirka):
		print("=",end="")
	print("\n")
	modifikace = input('Zadejte cislo pozadovane modifikace (viz vyse)!\n')
	try:
		mod=int(modifikace)
		if mod<1 or mod>8:
			raise ValueError
	except ValueError:
		print('\nCHYBA: Nezadali jste cislo zadne z modifikaci!')
	else:
		#uprava obrazku ve spravne funkci
		obrjm='.'.join(obrsoubor.split('.')[0:-1])
		data=im.open(obrsoubor)
		obr=np.asarray(data)
		#1) zmenseni obrazku
		if mod==1:
			obrvys=zmenseni()
			modif="mensi"
		#2) otoceni obrazku
		if mod==2:
			obrvys=otoceni()
			modif="otoceny"
		#3) prevraceni obrazku
		if mod==3:
			obrvys=prevraceni()
			modif="prevraceny"
		#4) inverzni obrazek
		if mod==4:
			obrvys=inverze()
			modif="inverzni"
		#5) prevod do odstiu sedi
		if mod==5:
			obrvys=seda()
			modif="sedy"
		#6) zesvetleni
		if mod==6:
			obrvys=zesvetleni()
			modif="svetly"
		#7) ztmaveni
		if mod==7:
			obrvys=ztmaveni()
			modif="tmavy"
		#8) zvyrazneni hran
		if mod==8:
			obrvys=zvyrazneni()
			modif="zvyrazneny"

		#ulozeni vystupniho obrazku
		name=obrjm+"_"+modif+"."+form
		output=im.fromarray(obrvys,'RGB')
		output.save(name)

