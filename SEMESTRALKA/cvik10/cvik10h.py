#!/usr/bin/env python3

import numpy as np
from PIL import Image

#otevre obrazek a nacte pomoci pillow
img=Image.open("negativ.jpg")


#ulozi data obrazku do numpy matice - spravna velikost a 3 rgb slozky
data=np.asarray(img)
data.setflags(write=1)
gray=data
positive=255-data
positive[...,0:1]=0
positive[...,2:3]=0
fivecount=data.shape[0]//5
part1=positive[0:fivecount,...]
part2=positive[fivecount:fivecount*2,...]
part3=positive[fivecount*2:fivecount*3,...]
part4=positive[fivecount*3:fivecount*4,...]
part5=positive[fivecount*4:fivecount*5,...]


part1[...,1:2]=part1[...,1:2]//4
part2[...,1:2]=part2[...,1:2]//2

idx4=(part4>255//2)
part4[...,1:2]=part4[...,1:2]*2
part4[idx4]=255

idx5=(part5>255//4)
part5[...,1:2]=part5[...,1:2]*4
part5[idx5]=255

photo=np.concatenate((part1,part2,part3, part4,part5), axis=0)


#ulozeni noveho obrazku
output=Image.fromarray(photo,'RGB')
output.save("photo.jpg")
