#!/usr/bin/env python3

import numpy as np
from PIL import Image

#otevre obrazek a nacte pomoci pillow
img=Image.open("kvetina.jpg")

#vypise info o obrazku
#print(img) 

#ulozi data obrazku do numpy matice - spravna velikost a 3 rgb slozky
data=np.asarray(img)

#ztmaveni obrazku
dark=data//2


#ulozeni noveho obrazku
output=Image.fromarray(dark,'RGB')
output.save("dark.jpg")
