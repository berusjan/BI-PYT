#!/usr/bin/env python3

import numpy as np
from PIL import Image

#otevre obrazek a nacte pomoci pillow
img=Image.open("kvetina.jpg")


#ulozi data obrazku do numpy matice - spravna velikost a 3 rgb slozky
data=np.asarray(img)
data.setflags(write=1)
gray=data


#pravod do odstinu sedi
#...?
#Y′601=0.299*R′+0.587*G′+0.114*B′

for rad in (range(data.shape[0])):
	for sl in range(data.shape[1]):
		one=round(0.299*data[rad][sl][0]+0.587*data[rad][sl][1]+0.114*data[rad][sl][2])
		gray[rad][sl][0]=one
		gray[rad][sl][1]=one
		gray[rad][sl][2]=one
		#gray[rad][sl][0]=0.299*data[rad][sl][0]+0.587*data[rad][sl][1]+0.114*data[rad][sl][2]
		#gray[rad][sl][1]=0.299*data[rad][sl][0]+0.587*data[rad][sl][1]+0.114*data[rad][sl][2]
		#gray[rad][sl][2]=0.299*data[rad][sl][0]+0.587*data[rad][sl][1]+0.114*data[rad][sl][2]

#ulozeni noveho obrazku
output=Image.fromarray(gray,'RGB')
output.save("gray.jpg")
