#!/usr/bin/env python3

import numpy as np
from PIL import Image

#otevre obrazek a nacte pomoci pillow
img=Image.open("kvetina.jpg")

#vypise info o obrazku
print(img) 

#ulozi data obrazku do numpy matice - spravna velikost a 3 rgb slozky
data=np.asarray(img)

#ukazka formatu dat
print(data.shape)
print(data.dtype)
print(data[0,0])


#pravod pomoci vektoroveho odectu na negativ
negativ=255-data


#ulozeni noveho obrazku
output=Image.fromarray(negativ,'RGB')
output.save("negativ.jpg")
