#!/usr/bin/env python3

import numpy as np
from PIL import Image

COLOR=2
#otevre obrazek a nacte pomoci pillow
img=Image.open("kvetina.jpg")

#vypise info o obrazku
#print(img) 

#ulozi data obrazku do numpy matice - spravna velikost a 3 rgb slozky
data=np.asarray(img)


#vypnuti daneho kanalu
data.setflags(write=1)
colors=data
idx=(data>155)
colors[...,COLOR:COLOR+1]=0
#print(colors)



#ulozeni noveho obrazku
output=Image.fromarray(colors,'RGB')
output.save("colors.jpg")
