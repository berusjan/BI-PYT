#!/usr/bin/env python3

import numpy as np
from PIL import Image

#otevre obrazek a nacte pomoci pillow
img=Image.open("kvetina.jpg")


#ulozi data obrazku do numpy matice - spravna velikost a 3 rgb slozky
data=np.asarray(img)



#odebrani kazdeho druheho pixelu
small=data[::2,::2,::]
#print(small.shape)



#ulozeni noveho obrazku
output=Image.fromarray(small,'RGB')
output.save("small.jpg")
