#!/usr/bin/env python3

import numpy as np
from PIL import Image

#otevre obrazek a nacte pomoci pillow
img=Image.open("kvetina.jpg")

#vypise info o obrazku
#print(img) 

#ulozi data obrazku do numpy matice - spravna velikost a 3 rgb slozky
data=np.asarray(img)


#zesvetleni obrazku
data.setflags(write=1)
light=data*2
idx=(data>255//2)
light[idx]=255
#pri nasobeni mi to nechce prelezt pres 255 - nejak se to cykli ????
#light=data+100
#idx=(data>155)
#light[idx]=255

#ulozeni noveho obrazku
output=Image.fromarray(light,'RGB')
output.save("light.jpg")
