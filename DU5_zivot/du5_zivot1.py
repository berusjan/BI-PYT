#!/usr/bin/env python3

import numpy as np
import random
import os
import time
import sys

###############

def novy_stav(mat,rad,sl):
	bunka=mat[rad][sl]
	rad+=1
	sl+=1
	matokr=np.insert(mat,0,values=0,axis=0)
	matokr=np.insert(matokr,0,values=0,axis=1)
	matokr=np.concatenate((matokr,np.zeros((matokr.shape[0],1),dtype=int)), axis=1)
	matokr=np.concatenate((matokr,np.zeros((1,matokr.shape[1]),dtype=int)), axis=0)
	sousedi=matokr[rad-1:rad+2,sl-1:sl+2]
	stav=0
	suma=0
	for rad in range(3):
		for sl in range(3):
			suma+=sousedi[rad][sl]
	suma-=bunka
	if (bunka==1 and (suma==2 or suma==3)) or (bunka==0 and suma==3):
		stav=1
	return stav

##############


if len(sys.argv) !=3:
	print("USAGE:",sys.argv[0],"vystupni_soubor pocet_kol")
	sys.exit()

cols, lines = os.get_terminal_size(0)
print('\033[2J')

FILENAME=sys.argv[1]

KOL=int(sys.argv[2])
if KOL < 1:
	print("Pocet kol neni prirozene cislo!")
	sys.exit()

#nahodne inicializovana matice
matnova=np.full([lines,cols],0)
mat=np.full([lines,cols],0)
for rad in range(lines):
	for sl in range(cols):
		r=random.random()
		if r>=0.5:
			(mat[rad][sl])=1

#otevreni zapisoveho souboru
if os.path.isfile(FILENAME):
	os.remove(FILENAME)
with open(FILENAME,mode='w',encoding='utf-8') as vystup:
	#pocatecni stav
	data="Pocatecni stav:\n"
	for rad in range(lines):
		for sl in range(cols):
			if mat[rad][sl]==1:
				print('\033[?25l\033[{};{}H\033[47m \033[0m'.format(rad+1,sl+1),end="")
			else:
				print('\033[?25l\033[{};{}H\033[40m \033[0m'.format(rad+1,sl+1),end="")
			data+=str(mat[rad][sl])
		data+="\n"
	print('\033[?25l\033[{};{}H\033[0m'.format(lines,cols),end="",flush=True)

	#hra
	for i in range(1,KOL+1):
		data+="\n----------------------\n"
		data+=str(i)+". kolo:\n"
		time.sleep(1)
		for rad in range(lines):
			for sl in range(cols):
				matnova[rad][sl]=novy_stav(mat,rad,sl)
				if matnova[rad][sl]==1:
					print('\033[?25l\033[{};{}H\033[47m \033[0m'.format(rad+1,sl+1),end="")
				else:
					print('\033[?25l\033[{};{}H\033[40m \033[0m'.format(rad+1,sl+1),end="")
				data+=str(matnova[rad][sl])
			data+="\n"
		print('\033[?25l\033[{};{}H\033[0m'.format(lines,cols),end="",flush=True)
		mat=matnova
	vystup.write(data)

print('\033[{};{}H\033[0m\033[?25h'.format(lines,cols))

