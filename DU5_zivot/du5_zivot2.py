#!/usr/bin/env python3

import numpy as np
import os
import time
import sys

###############

def novy_stav(mat,rad,sl):
	bunka=mat[rad][sl]
	rad+=1
	sl+=1
	matokr=np.insert(mat,0,values=0,axis=0)
	matokr=np.insert(matokr,0,values=0,axis=1)
	matokr=np.concatenate((matokr,np.zeros((matokr.shape[0],1),dtype=int)), axis=1)
	matokr=np.concatenate((matokr,np.zeros((1,matokr.shape[1]),dtype=int)), axis=0)
	sousedi=matokr[rad-1:rad+2,sl-1:sl+2]
	stav=0
	suma=0
	for rad in range(3):
		for sl in range(3):
			suma+=sousedi[rad][sl]
	suma-=bunka
	if (bunka==1 and (suma==2 or suma==3)) or (bunka==0 and suma==3):
		stav=1
	return stav

##############


if len(sys.argv) !=4:
	print("USAGE:",sys.argv[0]," vstupni_soubor vystupni_soubor pocet_kol")
	sys.exit()


FILENAMEOUT=sys.argv[2]
FILENAMEIN=sys.argv[1]
if os.path.exists(FILENAMEIN):
	if not os.path.isfile(FILENAMEIN):
		print('Soubor {} nenalezen'.format(FILENAMEIN))
		sys.exit()
else:
	print('Soubor {} nenalezen.'.format(FILENAMEIN))
	sys.exit()

KOL=int(sys.argv[3])
if KOL < 1:
	print("Pocet kol neni prirozene cislo!")
	sys.exit()


#nacteni dat ze souboru
read=[]
with open(FILENAMEIN,mode='r',encoding='utf-8') as vstup:
	for radek in vstup:
		read.append(radek)
	vstup.close()
cols=(len(read[0])-1)
lines=len(read)
mat=np.full([lines,cols],0)
j=0
for radek in read:
	i=0
	for sl in range(0,cols):
		string=radek[sl]
		mat[j][i]=int(string)
		i+=1
	j+=1


oldcols, oldlines = os.get_terminal_size(0)
#oldcols=80
#oldlines=24
sizestr="resize -s "+str(lines)+" "+str(cols)+" > /dev/null"
os.system(sizestr)


print('\033[2J')

matnova=np.full([lines,cols],0)
#otevreni zapisoveho souboru
if os.path.isfile(FILENAMEOUT):
	os.remove(FILENAMEOUT)
with open(FILENAMEOUT,mode='w',encoding='utf-8') as vystup:
	#pocatecni stav
	data="Pocatecni stav:\n"
	for rad in range(lines):
		for sl in range(cols):
			if mat[rad][sl]==1:
				print('\033[?25l\033[{};{}H\033[47m \033[0m'.format(rad+1,sl+1),end="")
			else:
				print('\033[?25l\033[{};{}H\033[40m \033[0m'.format(rad+1,sl+1),end="")
			data+=str(mat[rad][sl])
		data+="\n"
	print('\033[?25l\033[{};{}H\033[0m'.format(lines,cols),end="",flush=True)

	#hra
	for i in range(1,KOL+1):
		data+="\n----------------------\n"
		data+=str(i)+". kolo:\n"
		time.sleep(1)
		for rad in range(lines):
			for sl in range(cols):
				matnova[rad][sl]=novy_stav(mat,rad,sl)
				if matnova[rad][sl]==1:
					print('\033[?25l\033[{};{}H\033[47m \033[0m'.format(rad+1,sl+1),end="")
				else:
					print('\033[?25l\033[{};{}H\033[40m \033[0m'.format(rad+1,sl+1),end="")
				data+=str(matnova[rad][sl])
			data+="\n"
		print('\033[?25l\033[{};{}H\033[0m'.format(lines,cols),end="",flush=True)
		mat=matnova
	vystup.write(data)

print('\033[{};{}H\033[0m\033[?25h'.format(lines,cols))

if oldlines > lines or oldcols > cols:
	sizestr="resize -s "+str(oldlines)+" "+str(oldcols)+" > /dev/null"
	os.system(sizestr) 



