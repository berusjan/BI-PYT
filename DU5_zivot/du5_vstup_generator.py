#!/usr/bin/env python3

import numpy as np
import random
import os

FILENAME = input('Zadej jmeno souboru, kam se vstup pro hru zivot ulozi:\n')
y = input('Zadej pocet radek v matici pro hru (max 50):\n')
try:
	lines=int(y)
	if lines < 1 or lines > 50:
		raise ValueError
except ValueError:
	print('Nezadal jsi prirozene cislo nebo je cislo prislis velke!')
else:
	x = input('Zadej pocet sloupcu v matici pro hru (max 150):\n')
	try:
		cols=int(x)
		if cols < 1 or cols>150:
			raise ValueError
	except ValueError:
		print('Nezadal jsi prorizene cislo nebo je cislo prilis velke!')
	else:
		#nahodne inicializovana matice
		matnova=np.full([lines,cols],0)
		mat=np.full([lines,cols],0)
		for rad in range(lines):
			for sl in range(cols):
				r=random.random()
				if r>=0.5:
					(mat[rad][sl])=1

		#zapis dat do souboru
		if os.path.isfile(FILENAME):
			os.remove(FILENAME)
		with open(FILENAME,mode='w',encoding='utf-8') as vstup:
			#pocatecni stav
			data=""
			for rad in range(lines):
				for sl in range(cols):
					data+=str(mat[rad][sl])
				data+="\n"
			vstup.write(data)

