#!/usr/bin/env python3

import string
import sys

print(string.ascii_uppercase)

hes="azvfgbnhVFGBNHbghnBGNH567.bgh"
heslo=hes.upper()
print(hes)
print(heslo)


h=heslo[0]
print(h)
c=ord(h)
print(c)

h=heslo[1]
print(h)
c=ord(h)
print(c)
z=chr(c)
print(z)


DICTFILE="syn2010_word.vyber-ascii.txt"
slovnik=[]
with open(DICTFILE,encoding='utf-8') as slov:
	data=slov.read()
	data=data.upper()
	slovnik = data.splitlines()
	slov.close()

slovo=sys.argv[1]


if slovo not in slovnik:
	print(slovo," neni!")
else:
	print(slovo," je ve slovniku!")

