#!/usr/bin/env python3

import string

zaklad=ord('A')
konec=ord('Z')


def cesarovaSifra_encrypt(h,t):
	hc=ord(h)
	tc=ord(t)
	posun=hc-zaklad
	pos=tc+posun
	if pos>konec:
		pos=pos-konec+zaklad-1
	znak=chr(pos)
	return znak


def cesarovaSifra_decrypt(h,t):
	hc=ord(h)
	tc=ord(t)
	posun=hc-zaklad
	pos=tc-posun
	if pos<zaklad:
		pos=konec-(zaklad-pos)+1
	znak=chr(pos)
	return znak


hes="terapeutky"
lenhes=len(hes)
hesl=hes.upper()
with open("text.txt",encoding='utf-8') as data:
	text = data.read()
	data.close()
DICTFILE="syn2010_word.vyber-ascii.txt"
slovnik=[]
with open(DICTFILE,encoding='utf-8') as slov:
	data=slov.read()
	data=data.upper()
	slovnik = data.splitlines()
	slov.close()

text=text.upper()
lentext=len(text)
heslo=""
vysl=""
cnt=0
slovo=""
for i in range(0,lentext):
	heslo+=hesl[cnt]
	if (text[i] < 'A') or (text[i]) > 'Z':
		cnt-=1
	cnt+=1
	if cnt==lenhes:
		cnt=0

for i in range(0,lentext):
	if (text[i] < 'A') or (text[i]) > 'Z':
		znak=text[i]
	else:
		znak=cesarovaSifra_decrypt(heslo[i], text[i])
	vysl=vysl+znak
	if znak==' ' or znak=='\n':
		if slovo not in slovnik and len(slovo)>0:
			print(slovo)
		slovo=""
	else:
		if (znak >= 'A') and (znak <= 'Z'):
			slovo+=znak

#############
#OKOUNENI
#DIVENI
#NEODRIKAVA
#PULMINUTOVYCH
#OSVETLIME
#SJETO
#ZAPLACETE
#NEVYNASEJI
#VZDEJTEZ
#JIHNOUCI
#NECHCETELI



