#!/usr/bin/env python3

import string

zaklad=ord('A')
konec=ord('Z')


def cesarovaSifra_encrypt(h,t):
	hc=ord(h)
	tc=ord(t)
	posun=hc-zaklad
	pos=tc+posun
	if pos>konec:
		pos=pos-konec+zaklad-1
	znak=chr(pos)
	return znak


def cesarovaSifra_decrypt(h,t):
	hc=ord(h)
	tc=ord(t)
	posun=hc-zaklad
	pos=tc-posun
	if pos<zaklad:
		pos=konec-(zaklad-pos)+1
	znak=chr(pos)
	return znak



mode = input('Chces text zasifrovat (encrypt) nebo desifrovat (decrypt)? Zadej e/d!\n')
try:
	if mode == 'E':
		mode='e'
	if mode == 'D':
		mode='d'
	if (mode != 'e') and (mode !='d'):
		raise ValueError
except ValueError:
	print('Nevybral jsi spravny mod! Moznosti jsou e/d.')
else:
	hes = input('Zadej sifrovaci klic! (Klic se musi skladat pouze z pismen.)\n')
	lenhes=len(hes)
	hesl=hes.upper()
	try:
		for i in range(0,lenhes):
			if (hesl[i] < 'A') or (hesl[i]) > 'Z':
				raise ValueError
	except ValueError:
		print('Klic obsahuje nepovolene znaky! (Klic se musi skladat pouze z pismen.)')
	else:
		if mode=='e':
			tex = input('Zadej text k zasifrovani!\n')
		else:
			tex = input('Zadej zasifrovany text k rozsifrovani!\n')
		text=tex.upper()
		lentext=len(text)
		heslo=""
		vysl=""
		cnt=0
		for i in range(0,lentext):
			heslo+=hesl[cnt]
			if (text[i] < 'A') or (text[i]) > 'Z':
				cnt-=1
			cnt+=1
			if cnt==lenhes:
				cnt=0
		if mode=='e':
			for i in range(0,lentext):
				if (text[i] < 'A') or (text[i]) > 'Z':
					znak=text[i]
				else:
					znak=cesarovaSifra_encrypt(heslo[i], text[i])
				vysl=vysl+znak
			print("Zasifrovany text je:")
			print(vysl)
		else:
			for i in range(0,lentext):
				if (text[i] < 'A') or (text[i]) > 'Z':
					znak=text[i]
				else:
					znak=cesarovaSifra_decrypt(heslo[i], text[i])
				vysl=vysl+znak
			print("Rozsifrovany text je:")
			print(vysl)


