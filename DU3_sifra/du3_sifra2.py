#!/usr/bin/env python3


DICTFILE="syn2010_word.vyber-ascii.txt"
DATAFILE="text.txt"
DATAOUT="vystup.txt"

def cesarovaSifra_decrypt(h,t):
	zaklad=ord('A')
	konec=ord('Z')
	hc=ord(h)
	tc=ord(t)
	posun=hc-zaklad
	pos=tc-posun
	if pos<zaklad:
		pos=konec-(zaklad-pos)+1
	znak=chr(pos)
	return znak


slovnik=[]
with open(DICTFILE,encoding='utf-8') as slov:
	data=slov.read()
	data=data.upper()
	slovnik = data.splitlines()
	slov.close()

with open(DATAFILE,encoding='utf-8') as data:
	text = data.read()
	data.close()

text=text.upper()
lentext=len(text)

#it=0
for hesl in slovnik:
	#it2=0
	#it+=1
	flag=1
	lenhes=len(hesl)
	hesl=hesl.upper()
	heslo=""
	vysl=""
	slovo=""
	cnt=0
	for i in range(0,lentext):
		heslo+=hesl[cnt]
		if (text[i] < 'A') or (text[i] > 'Z'):
			cnt-=1
		cnt+=1
		if cnt==lenhes:
			cnt=0
	for i in range(0,lentext):
		if (text[i] < 'A') or (text[i] > 'Z'):
			znak=text[i]
		else:
			znak=cesarovaSifra_decrypt(heslo[i], text[i])
		vysl+=znak
		if znak==' ' or znak=='\n':
			#it2+=1
			if slovo not in slovnik and len(slovo)>0:
				#print(it,it2,slovo)
				flag=0
				break
			slovo=""
		else:
			if (znak >= 'A') and (znak <= 'Z'):
				slovo+=znak
	if flag:
		print(vysl,end="")
		with open(DATAOUT,mode='wt',encoding='utf-8') as vystup:
			vystup.write(vysl)
			vystup.close()
		break



