#!/usr/bin/env python3

def jePrvocislo(n):
	for i in range(2,n):
		if n%i==0:
			return False
	return True



flag=1
n = input('Zadej pocet prirozenych cisel, ktere je mozne odmocnit: ')
try:
	val=int(n)
	if val <= 0:
		raise ValueError
	for i in range(1,val):
		if i*i==val:
			flag=0
			rows=i
			break
	if flag:
		raise ValueError
except ValueError:
	print('Nezadal jsi prirozene cislo, ktere je mozne odmocnit!')

else:
	ciph=len(n)
	suma=2
	p=1
	h=0
	l=0
	d=0
	if val%2==0:
		flag=1

	matrix = [[0 for j in range(rows)] for i in range(rows)]

	if flag:
		y=rows//2
		x=rows//2-1
		matrix[y][x]=str(1).zfill(ciph)+" "
	else:
		x=rows//2
		y=rows//2
		matrix[y][x]=str(1).zfill(ciph)+" "



	for j in range(2,rows+1):
		maxim=j*j-(j-1)*(j-1)
		if p:
			x=x+1
			if jePrvocislo(suma):
				matrix[y][x]='\033[31m'+'\033[1m'+str(suma).zfill(ciph)+'\033[0m'+" "
			else:
				matrix[y][x]=str(suma).zfill(ciph)+" "
			suma+=1
			#print(suma," ap")
			p=0
			h=1
		else:
			x=x-1
			if jePrvocislo(suma):
				matrix[y][x]='\033[31m'+'\033[1m'+str(suma).zfill(ciph)+'\033[0m'+" "
			else:
				matrix[y][x]=str(suma).zfill(ciph)+" "
			suma+=1
			#print(suma," al")
			l=0
			d=1

		if h:
			for i in range(0,maxim//2):
				y=y-1
				if jePrvocislo(suma):
					matrix[y][x]='\033[31m'+'\033[1m'+str(suma).zfill(ciph)+'\033[0m'+" "
				else:
					matrix[y][x]=str(suma).zfill(ciph)+" "
				suma+=1
				#print(suma," bh")
			h=0
			l=1
		else:
			for i in range(0,maxim//2):
				y=y+1
				if jePrvocislo(suma):
					matrix[y][x]='\033[31m'+'\033[1m'+str(suma).zfill(ciph)+'\033[0m'+" "
				else:
					matrix[y][x]=str(suma).zfill(ciph)+" "
				suma+=1
				#print(suma," bd")
				d=0
				p=1

		if p:
			for i in range(0,maxim//2):
				x=x+1
				if jePrvocislo(suma):
					matrix[y][x]='\033[31m'+'\033[1m'+str(suma).zfill(ciph)+'\033[0m'+" "
				else:
					matrix[y][x]=str(suma).zfill(ciph)+" "
				suma+=1
				#print(suma," cp")

		else:
			for i in range(0,maxim//2):
				x=x-1
				if jePrvocislo(suma):
					matrix[y][x]='\033[31m'+'\033[1m'+str(suma).zfill(ciph)+'\033[0m'+" "
				else:
					matrix[y][x]=str(suma).zfill(ciph)+" "
				suma+=1
				#print(suma," cl")


	#konecny tisk
	for j in range(0,rows):
		for i in range(0,rows):
			print(matrix[j][i],end="")
		print("")


