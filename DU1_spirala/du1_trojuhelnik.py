#!/usr/bin/env python3

def jePrvocislo(n):
	for i in range(2,n):
		if n%i==0:
			return False
	return True



import os
columns, rows = os.get_terminal_size(0)

n = input('Zadej pocet prirozenych cisel! ')
try:
	val=int(n)
	if val <= 0:
		raise ValueError
except ValueError:
	print('Nezadal jsi prirozene cislo!')

else:
	cnt=0
	suma=2
	ciph=len(n)

	for i in range(2,val+1):
		cnt+=1
		if cnt==suma:
			cnt=0
			suma=suma+1


	cnt=0
	suma=2
	row=""
	print("1".zfill(ciph).center(80))
	for i in range(2,val+1):
		if jePrvocislo(i):
			row+='\033[31m'+'\033[1m'+str(i).zfill(ciph)+'\033[37m'+'\033[0m'+" "
		else:
			row+=str(i).zfill(ciph)+" "
		cnt+=1
		if cnt==suma:
			filling=(columns-(cnt*(ciph+1)))//2
			print(" "*filling+row)
			row=""
			cnt=0
			suma=suma+1
	if row:
		filling=(columns-(cnt*(ciph+1)))//2
		print(" "*filling+row)









