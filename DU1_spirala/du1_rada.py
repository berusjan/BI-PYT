#!/usr/bin/env python3

def jePrvocislo(n):
	for i in range(2,n):
		if n%i==0:
			return False
	return True




n = input('Zadej pocet prirozenych cisel! ')
try:
	val=int(n)
	if val <= 0:
		raise ValueError
except ValueError:
	print('Nezadal jsi prirozene cislo!')

else:
	print("1 ",end="")
	for i in range(2,val+1):
		if jePrvocislo(i):
			print('\033[31m'+'\033[1m'+"",i," "+'\033[37m'+'\033[0m',end="")
		else:
			print(i," ",end="")

	print("")









