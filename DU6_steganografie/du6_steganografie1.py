#!/usr/bin/env python3

# MUJ KOD (slovnik vytvoren v souboru kod.py):
#DEC	RGB RGB		HOD		|		DEC		RGB RGB		HOD	
#0		000 000		0		|		32		100 000		W	
#1		000 001		1		|		33		100 001		X	
#2		000 010		2		|		34		100 010		Y	
#3		000 011		3		|		35		100 011		Z	
#4		000 100		4		|		36		100 100		 	mezera
#5		000 101		5		|		37		100 101		,	carka
#6		000 110		6		|		38		100 110		.	tecka
#7		000 111		7		|		39		100 111		?	otaynik
#8		001 000		8		|		40		101 000		!	vykricnik
#9		001 001		9		|		41		101 001		:	dvojtecka
#10		001 010		A		|		42		101 010		;	strednik
#11		001 011		B		|		43		101 011		“	uvozovky
#12		001 100		C		|		44		101 100		-	pomlcka
#13		001 101		D		|		45		101 101		_	podtrzitko
#14		001 110		E		|		46		101 110		(	leva zavorka
#15		001 111		F		|		47		101 111		)	prava zavorka
#16		010 000		G		|		48		110 000		{	leva sl. Zavorka
#17		010 001		H		|		49		110 001		}	prava sl. Zavorka
#18		010 010		I		|		50		110 010		[	leva hr. Zavorka
#19		010 011		J		|		51		110 011		]	prava hr. Zavorka
#20		010 100		K		|		52		110 100		<	leva sp. Zavorka
#21		010 101		L		|		53		110 101		>	prava sp. Zavorka
#22		010 110		M		|		54		110 110		@	zavinac
#23		010 111		N		|		55		110 111		*	hvezdicka
#24		011 000		O		|		56		111 000		/	lomeno
#25		011 001		P		|		57		111 001		+	plus
#26		011 010		Q		|		58		111 010		%	procento
#27		011 011		R		|		59		111 011		#	hash
#28		011 100		S		|		60		111 100		&	ampersnad
#29		011 101		T		|		61		111 101		^	striska
#30		011 110		U		|		62		111 110		$	dolar
#31		011 111		V		|		63		111 111		'	apostrof
					



import os
import sys
import numpy as np
from PIL import Image as im
import kod



mode = input('Chcete text zasifrovat (encrypt) nebo desifrovat (decrypt)? Zadejte e/d!\n')
try:
	if mode == 'E':
		mode='e'
	if mode == 'D':
		mode='d'
	if (mode != 'e') and (mode !='d'):
		raise ValueError
except ValueError:
	print('Nevybral jste spravny mod! Moznosti jsou e/d.')
else:
	obrsoubor = input("Zadejte cestu ke vstupnimu obrazku!\n")
	try:
		if os.path.exists(obrsoubor):
			if not os.path.isfile(obrsoubor):
				raise FileExistsError
		else:
			raise FileExistsError
		form=obrsoubor.split('.')[-1]
		if form!='jpg' and form!='JPG':
			raise TypeError
	except FileExistsError:
		print("\nCHYBA: Soubor '{}' neexistuje.".format(obrsoubor))
	except TypeError:
		print("\nCHYBA: Soubor '{}' neni v pozadovanem formatu JPEG.".format(obrsoubor))
	else:
		obrjm='.'.join(obrsoubor.split('.')[0:-1])
		data=im.open(obrsoubor)
		obr=np.asarray(data)
		obr.setflags(write=1)
		#print(obr)
		if mode=='e':
			text = input('Zadejte text k zasifrovani!\n')
			text=text.upper()
			bity=[]
			for z in text:
				if not z in kod.znaky:
					print("\nCHYBA: Znak '{}' neni soucasti me abecedy.".format(z))
					sys.exit()
				bity+=[format(kod.znaky[z],'06b')]
			sl=0
			rad=0
			cnt=0
			for i in bity:
				obr[rad][sl][0]=(obr[rad][sl][0]&254)|int(i[0])
				obr[rad][sl][1]=(obr[rad][sl][1]&254)|int(i[1])
				obr[rad][sl][2]=(obr[rad][sl][2]&254)|int(i[2])
				if cnt<5:
					print(obr[rad][sl][0],obr[rad][sl][1],obr[rad][sl][2])
				sl+=1
				if sl==obr.shape[0]:
					rad+=1
					sl=0
					if rad==obr.shape[1]:
						print("\nVAROABANI: Text je prilis dlouhy a nevejde se do obrazku cely.".format(z))
						break
				obr[rad][sl][0]=(obr[rad][sl][0]&254)|int(i[3])
				obr[rad][sl][1]=(obr[rad][sl][1]&254)|int(i[4])
				obr[rad][sl][2]=(obr[rad][sl][2]&254)|int(i[5])
				if cnt<5:
					print(obr[rad][sl][0],obr[rad][sl][1],obr[rad][sl][2])
				sl+=1
				cnt+=1
				if sl==obr.shape[0]:
					rad+=1
					sl=0
					if rad==obr.shape[1]:
						print("\nVAROABANI: Text je prilis dlouhy a nevejde se do obrazku cely.".format(z))
						break
			#ulozeni vystupniho obrazku
			name=obrjm+"_sifra."+form
			output=im.fromarray(obr, "RGB")
			output.save(name,"jpeg")
			print("===================\n",obr)
			data=im.open(name)
			obr1=np.asarray(data)
			print("===================\n",obr1)
		else:
			text=""
			cislo=""
			vys=1
			for rad in (range(obr.shape[0])):
				for sl in range(obr.shape[1]):
					if vys==1:
						cislo+=str(obr[rad][sl][0]&1)
						cislo+=str(obr[rad][sl][1]&1)
						cislo+=str(obr[rad][sl][2]&1)
						vys=0
					else:
						cislo+=str(obr[rad][sl][0]&1)
						cislo+=str(obr[rad][sl][1]&1)
						cislo+=str(obr[rad][sl][2]&1)
						#zapis
						c=int(cislo,2)
						text+=kod.cisla[c]
						vys=1
						cislo=""
			print(text)

						











