#!/usr/bin/env python3



n = input('Zadej pocet policek na ose sachovnice: ')
try:
	val=int(n)
	if val <= 0:
		raise ValueError
except ValueError:
	print('Nezadal jsi prirozene cislo!')
else:
	with open('sachy_p4_XxX.pbm', 'bw') as obr:
		# hlavička
		sval=str(val)
		obr.write(b'P4\n')
		obr.write(bytes(sval,'windows-1250'))
		obr.write(b' ')
		obr.write(bytes(sval,'windows-1250'))
		obr.write(b'\n')
		# data
		data = bytearray()
		if val%8==0:
			zb=0
		else:
			zb=8-(val%8)
		for i in range(val):
			byt=""
			cnt=0
			for j in range(val+zb):
				pole = (i + j) % 2
				if pole>=val:
					pole=0
				byt+=str(pole)
				cnt+=1
				if cnt==8:
					data.append(int(byt,2))
					cnt=0
					byt=""
		obr.write( data )
