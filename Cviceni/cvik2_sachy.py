#!/usr/bin/env python3

import sys
import os

if len(sys.argv) !=2:
	print("USAGE:",sys.argv[0],"N")
	sys.exit()

try:
	rows=int(sys.argv[1])
	if rows <= 0:
		raise ValueError
except ValueError:
	print('Nezadal jsi prirozene cislo!')
	print("USAGE:",sys.argv[0],"N")
else:
	cols, lines = os.get_terminal_size(0)
	if rows*2>cols or rows>lines:
		print("Sachovnice je prilis velka, zadej mensi cislo!")
		print("USAGE:",sys.argv[0],"N")
		sys.exit()


	print('\033[2J')
	cerna=1
	cervena=0
	for pole in range(1,rows*rows+1):
		#print(pole)
		if cerna:
			print('\033[44m'+"  ",end="")
			cerna=0
			cervena=1
		else:
			print('\033[41m'+"  ",end="")
			cervena=0
			cerna=1
		if pole%rows==0:
			print('\033[0m'+"")
			if rows%2==0:
				if cerna:
					cerna=0
					cervena=1
				else:
					cervena=0
					cerna=1


