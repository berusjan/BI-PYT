#!/usr/bin/env python3

import struct
import zlib
#import sys
#print("Native byteorder: ", sys.byteorder)

print("STRUKTURA:")


with open('cvik8_sachovnice.png', 'rb') as img:
	head = img.read(8)
	if head != b'\x89PNG\r\n\x1a\n':
		print("Spatna hlavicka") # pozdeji predelat na vyjimku
		exit(0)
	else:
		print("\theader:",head)

	print("\tchunks:")
	data=b''
	while True:
		chlenb=img.read(4)
		chlen=struct.unpack('>I',chlenb)[0]
		chtype=img.read(4)
		chdata=img.read(chlen)
		chcrcb=img.read(4)
		chcrc=struct.unpack('>I',chcrcb)[0]
		if chcrc!=zlib.crc32(chtype+chdata):
			print("Spatny crc v chunku typu:",chtype) # pozdeji predelat na vyjimku
			exit(0)
		print("\t\t{\n\t\t'length':",chlenb,"\n\t\t'type':",chtype,"\n\t\t'data':",chdata,"\n\t\t'CRC':",chcrcb,"\n\t\t}")
		if chtype==b'IHDR':
			hdata=chdata
		if chtype==b'IDAT':
			data+=chdata
		if chtype==b'IEND':
			break
	#print(data)
	#print(headerdata)
	print("\nIHDR:\n\t{")
	hwidthb=hdata[0:4]
	hwidth=struct.unpack('>I',hwidthb)[0]
	hheightb=hdata[4:8]
	hheight=struct.unpack('>I',hheightb)[0]
	hdepthb=b'\x00\x00\x00'
	hdepthb+=hdata[8:9]
	hdepth=struct.unpack('>I',hdepthb)[0]
	hcoltypeb=b'\x00\x00\x00'
	hcoltypeb+=hdata[9:10]
	hcoltype=struct.unpack('>I',hcoltypeb)[0]
	hcompressb=b'\x00\x00\x00'
	hcompressb+=hdata[10:11]
	hcompress=struct.unpack('>I',hcompressb)[0]
	hfilterb=b'\x00\x00\x00'
	hfilterb+=hdata[10:11]
	hfilter=struct.unpack('>I',hfilterb)[0]
	hinterlaceb=b'\x00\x00\x00'
	hinterlaceb+=hdata[10:11]
	hinterlace=struct.unpack('>I',hinterlaceb)[0]
	print("\t'width':",hwidth,"\n\t'height':",hheight,"\n\t'bit depth':",hdepth,"\n\t'colour type':",hcoltype,"\n\t'compression method':",hcompress,"\n\t'filter method':",hfilter,"\n\t'interlace method':",hinterlace,"\n\t}")

	print("\nIDAT:\n  ",data)

	decomdata=zlib.decompress(data)
	print("\nIDAT – rozkomprimovany:\n  ",decomdata)

	print("\nIDAT – zafiltrované scanlines ve tvaru '(filtr, [(RGB-hodnoty jednotlivých pixelů), ])':")
	print("  [")
	for h in range(hheight):
		fn=h*hwidth*3+h
		print("    (",decomdata[fn],", [",end="")
		for w in range(1,(hwidth)*3,3):
			print("(",decomdata[fn+w],",",decomdata[fn+w+1],",",decomdata[fn+w+2],")",end="")
		print("] )")
	print("  ]")

	print("\nIDAT – odfiltrované pixely (tady je to jednoduché, když je filtr 0 ^_^):")
	print("  [")
	for h in range(hheight):
		fn=h*hwidth*3+h
		print("    [",end="")
		for w in range(1,(hwidth)*3,3):
			print("(",decomdata[fn+w],",",decomdata[fn+w+1],",",decomdata[fn+w+2],")",end="")
		print("]")
	print("  ]")



with open('cvik8_sachovnice_moje.ppm', 'w') as vystup:
	# hlavička
	vystup.write('P3\n')
	vystup.write(str(hheight) + ' ' + str(hwidth) + '\n')
	vystup.write('255\n')
	# data
	obr = ''
	for h in range(hheight):
		fn=h*hwidth*3+h
		for w in range(1,(hwidth)*3,3):
			obr+=str(decomdata[fn+w])+' '+str(decomdata[fn+w+1])+' '+str(decomdata[fn+w+2])+' '
		obr+='\n'
	obr=obr[:-1]
	vystup.write(obr)



with open('cvik8_sachovnice_moje.ppm', 'r') as vystup:
	pr=vystup.read()
	print(pr)

