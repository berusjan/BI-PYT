#!/usr/bin/env python3
# -*- coding: utf-8 -*-



print('\nŘETĚZCE')
# řetězec
# ~ v Pythonu'u 3.x je unicodový
retezec = 'Příliš žluťoučký kůň úpěl ďábelské ódy.'
print( len(retezec) )
print( retezec.count('l') )



print('\nPRŮCHOD PO SEKVENČNÍCH TYPECH')
# průchod po sekvenčním typu (zde po řetězci)
for prvek in retezec:
    print( prvek, end='' )

print()   # odřádkování po předchozím výstupu

# průchod po sekvenčním typu (zde po řetězci) včetně získání indexu
# ~ plus ukázka formátování výstupu
str = 'řetězec'
for (i, prvek) in enumerate(str):
    print( '{0}: {1}'.format(i, prvek) )



print('\nCYKLUS FOR')
# for-cyklus určené délky, případně kroku
for i in range(10):
    print(i, end=' ')

print()   # odřádkování po předchozím výstupu

for i in range(5,10):
    print(i, end=' ')

print()   # odřádkování po předchozím výstupu

for i in range(0,10, 2):
    print(i, end=' ')

print()   # odřádkování po předchozím výstupu



print('\nIF')
# podmínka
bt = True
bf = False
if bt:
    print('1 - pravda')

if bt and bf:
    print('2 - pravda')
else:
    print('2 - nepravda')

n = 14
if (n % 3 == 0):
    print( '{0} dělitelné 3'.format(n) )
elif (n % 7 == 0):
    print( '{0} dělitelné 7'.format(n) )
else:
    print( '{0} není dělitelné ani 3, ani 7'.format(n) )



print('\nWHILE')
# while-cyklus
xs = [1,2,3,4,5]
while xs:
    print( xs.pop(), end=' ' )

print()   # odřádkování po předchozím výstupu



print('\nFUNKCE')
# funkce
def faktorial(n):
    f = 1
    for i in range(2,n+1):
        f *= i
    return f
print( '{0}! = {1}'.format(3, faktorial(3)) )
print( '{0}! = {1}'.format(4, faktorial(4)) )
print( '{0}! = {1}'.format(5, faktorial(5)) )
