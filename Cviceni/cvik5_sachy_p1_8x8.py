#!/usr/bin/env python3
 
with open('sachy_p1_8x8.pbm', 'w') as obr:
    # hlavička
    obr.write('P1\n')
    obr.write('8 8\n')
    # data
    data = ''
    for i in range(8):
        for j in range(8):
            pole = (i + j) % 2
            data += str(pole) + ' '
        data += '\n'
    obr.write( data )
