#!/usr/bin/env python3



n = input('Zadej pocet policek na ose sachovnice: ')
try:
	val=int(n)
	if val <= 0:
		raise ValueError
except ValueError:
	print('Nezadal jsi prirozene cislo!')
else:
	with open('sachy_p1_XxX.pbm', 'w') as obr:
		# hlavička
		obr.write('P1\n')
		obr.write(str(val))
		obr.write(' ')
		obr.write(str(val))
		obr.write('\n')
		# data
		data = ''
		for i in range(val):
		    for j in range(val):
		        pole = (i + j) % 2
		        data += str(pole) + ' '
		    data += '\n'
		obr.write( data )
