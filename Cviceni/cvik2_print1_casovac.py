#!/usr/bin/env python3

import time

for i in range(0,10):
	print(i,end="",flush=True)
	time.sleep(1)
	print('\b',end="",flush=True)
for i in range(10,61):
	print(i,end="",flush=True)
	time.sleep(1)
	print('\b\b',end="",flush=True)

#from time import sleep
#for i in range(0,61):
#	print(i,end="",flush=True)
#	sleep(1)
#	print('\b'*2,end="")
