#!/usr/bin/env python3

import random
import os
import time

cols, lines = os.get_terminal_size(0)
print('\033[2J')

for i in range(cols):
	print('\033[0;{}H\033[47m \033[0m'.format(i),end="")
print("")
for i in range(lines):
	print('\033[{};0H\033[47m \033[0m'.format(i))
for i in range(cols):
	print('\033[{};{}H\033[47m \033[0m'.format(lines-1,i),end="")
print("")
for i in range(lines):
	print('\033[{};{}H\033[47m \033[0m'.format(i,cols))

smer=random.randint(1,4)
x=random.randint(2,cols-2)
y=random.randint(2,lines-2)

while True:
	print('\033[?25l\033[{};{}H\033[41m \033[0m'.format(y,x),end="",flush=True)
	time.sleep(0.05)
	#print('\033[{};{}H\033[0m \033[0m'.format(y,x),end="",flush=True)
	if smer==1:	
		x=x+1
		y=y+1
		if x==cols-1:
			if y==lines-2:
				smer=4
				continue
			smer=3
			continue
		if y==lines-2:
			smer=2
			continue
	if smer==2:
		x=x+1	
		y=y-1
		if x==cols-1:
			if y==2:
				smer=3
				continue
			smer=4
			continue
		if y==2:
			smer=1
			continue
	if smer==3:
		x=x-1
		y=y+1
		if x==2:
			if y==lines-2:
				smer=2
				continue
			smer=1
			continue
		if y==lines-2:
			smer=4
			continue
	if smer==4:
		x=x-1	
		y=y-1
		if x==2:
			if y==2:
				smer=1
				continue
			smer=2
			continue
		if y==2:
			smer=3
			continue

