#!/usr/bin/env python3

import gzip
import string
import random
import matplotlib.pyplot as plt


#nacteni retezce z gzipu
with gzip.open('yersinia_pseudotuberculosis.fasta.gz', 'rt',encoding='utf-8') as fasta:
	genom = fasta.read()
lengen=len(genom)
start=0
for i in range(lengen):
	start+=1
	if genom[i]=='\n':
		break
genom=genom[start:]
genom=genom.replace("\n","")
lengen=len(genom)


#kontrola nacteni
#for i in range(100):
#	print(genom[i],end="")
print("Delka genomu: ",lengen)



#procenta g,c v celem genomu
c=genom.count('C')
g=genom.count('G')
procent=(g+c)/lengen
proc=procent*100
print("G=",g)
print("C=",c)
print("Procentualni zastoupeni G a C v celem genomu: "+"{0:.2f}".format(proc)+"%")



#rozdeleni genomu na seznam casti
genlist=list()
for i in range(0,lengen,100000):
	genlist.append(genom[i:i+100000])
lenlist=len(genlist)

#spocitani procent pro list
proclist=list()
maxim=0
minim=1
for j in range(lenlist):
	lenpart=len(genlist[j])
	c=genlist[j].count('C')
	g=genlist[j].count('G')
	proc=((g+c)/lenpart)
	proclist.append(proc)
	if maxim<proc:
		maxim=proc
	if minim>proc:
		minim=proc



#zamichani genomu a znovuspocitani procent
#genomrand=random.shuffle(genom)
liststring = list(genom)
random.shuffle(liststring)
genomrand = ''.join(liststring)

#rozdeleni noveho genomu na seznam casti
genlistrand=list()
for i in range(0,lengen,100000):
	genlistrand.append(genomrand[i:i+100000])
#spocitani procent pro list
proclistrand=list()
for j in range(lenlist):
	lenpart=len(genlist[j])
	c=genlistrand[j].count('C')
	g=genlistrand[j].count('G')
	proc=((g+c)/lenpart)
	proclistrand.append(proc)

#vytvoreni listu z celkoveho procenta
listcely=[procent for i in range(lenlist)]


#vykresleni grafu
plt.plot(proclist,'r+')
plt.plot(proclistrand,'g+')
plt.plot(listcely)
plt.axis([-1,lenlist+1,minim-0.005,maxim+0.005])
plt.xlabel('useky genomu')
plt.ylabel('procentualni zastoupeni v danem useku')
plt.show()


