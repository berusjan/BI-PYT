#!/usr/bin/env python3

import gzip
import string

#nacteni retezce z gzipu
with gzip.open('yersinia_pseudotuberculosis.fasta.gz', 'rt',encoding='utf-8') as fasta:
	genom = fasta.read()
lengen=len(genom)
start=0
for i in range(lengen):
	start+=1
	if genom[i]=='\n':
		break
genom=genom[start:]
genom=genom.replace("\n","")
lengen=len(genom)



#kontrola nacteni
#for i in range(100):
#	print(genom[i],end="")
print("Delka genomu: ",lengen)



#procenta g,c v celem genomu
a=0
t=0
g=0
c=0

for i in range(lengen):
	if genom[i]=='A':
		a+=1
	if genom[i]=='T':
		t+=1
	if genom[i]=='G':
		g+=1
	if genom[i]=='C':
		c+=1

proc=((g+c)/lengen)*100

print("G=",g)
print("C=",c)
print("Procentualni zastoupeni G a C v celem genomu: "+"{0:.2f}".format(proc)+"%")



#rozdeleni genomu na seznam casti
genlist=list()
for i in range(0,lengen,100000):
	genlist.append(genom[i:i+100000])
lenlist=len(genlist)

#spocitani procent pro list
proclist=list()
for j in range(lenlist):
	a=0
	t=0
	g=0
	c=0
	lenpart=len(genlist[j])
	for i in range(lenpart):
		if genlist[j][i]=='A':
			a+=1
		if genlist[j][i]=='T':
			t+=1
		if genlist[j][i]=='G':
			g+=1
		if genlist[j][i]=='C':
			c+=1

	proc=((g+c)/lenpart)*100
	proclist.append(proc)
	print("Procentualni zastoupeni G a C v genomu: "+"{0:.2f}".format(proc)+"%")






