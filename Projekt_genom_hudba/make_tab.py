#!/usr/bin/env python3

radky=[]
with open('aminokyseliny_abc.csv', 'rt',encoding='utf-8') as bilk:
	radky = bilk.readlines()


#první sloupeček těchto tabulátorem odsazených dat je název příslušné aminokyseliny, další jsou pak jednopísmenná zkratka, třípísmenná zkratka, následuje (čárkou a mezerou oddělený) seznam kódujících trojic bází (v podobě pro RNA, tedy s uracilem) a v posledním sloupečku je převod příslušné aminokyseliny do akordu
with open('tabulka.py',"wt",encoding='utf-8') as tab:
	tab.write("tab={\n")
	for radek in radky:
		sloup=[]
		parts=[]
		radek=radek.replace("\t","\n")
		sloup=radek.splitlines()
		if len(sloup)<5:
			continue
		parts=sloup[3].split()
		for p in parts:
			text=""
			text="'"+p[0:3]+"':'"+sloup[4]+"',\n"
			tab.write(text)
	tab.write("}\n\npop={\n")
	for radek in radky:
		sloup=[]
		parts=[]
		radek=radek.replace("\t","\n")
		sloup=radek.splitlines()
		if len(sloup)<5:
			continue
		parts=sloup[3].split()
		for p in parts:
			text=""
			text="'"+p[0:3]+"':'"+sloup[2]+"',\n"
			tab.write(text)
	tab.write("}")


