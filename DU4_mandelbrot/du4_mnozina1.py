#!/usr/bin/env python3
 
import numpy as np

MAXKROKU=20
SIRKA=400
PULVYSKA=150


mat=np.full([SIRKA,PULVYSKA],MAXKROKU)


with open('mnozina.pgm', 'w') as obr:

	obr.write('P2\n')
	velikost=str(SIRKA)+' '+str(PULVYSKA*2)
	obr.write(velikost)
	obr.write('\n20\n')
	
	strpole=""
	for rad in reversed(range(PULVYSKA)):
		for sl in range(SIRKA):
			kroku=0
			zn=0+0j
			c=complex((sl-250)/100,rad/100)
			absol=abs(zn)
			while absol<=2:
				zn1=pow(zn,2)+c
				absol=abs(zn1)
				kroku+=1
				zn=zn1
				if kroku==20:
					break
			mat[sl][rad]=MAXKROKU-kroku
			strpole+=str(mat[sl][rad])+" "
		strpole+="\n"
	for rad in range(PULVYSKA):
		for sl in range(SIRKA):
			strpole+=str(mat[sl][rad])+" "
		strpole+="\n"

	##Pozn: Misto druheho cyklu jsem chtela pole prepsat najednou, ale nepodarilo se mi najit fci, ktera pole prevede tak, aby se data zapsala do obrazku spravne.
	#strpole1=np.array_str(mat)
	#print(strpole1)
	#strpole1=strpole1.replace("["," ")
	#strpole1=strpole1.replace("]"," ")
	#print(strpole1)
	#strpole+=str(strpole1)

	obr.write( strpole )

