#!/usr/bin/env python3
 
import numpy as np

with open('pokus5.pgm', 'w') as obr:
    # hlavička
	obr.write('P2\n')
	obr.write('10 10\n15\n')
    # data
   # data = '0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0\n 0  3  3  3  3  0  0  7  0  0  7  0  0 11 11 11 11  0  0  0  0  0 15  0\n0  3  0  0  3  0  0  7  0  0  7  0  0 11  0  0 11  0  0  0  0  0 15  0\n0  3  3  3  3  0  0  7  7  7  7  0  0 11  0  0 11  0  0  0  0  0 15  0\n0  3  0  0  3  0  0  7  0  0  7  0  0 11  0  0 11  0  0 15  0  0 15  0\n0  3  0  0  3  0  0  7  0  0  7  0  0 11 11 11 11  0  0 15 15 15 15  0\n0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0\n'
	mat=np.full([10,10],15)
	mat[4][7]=0
	mat[4][3]=0
	mat[4][4]=0
	mat[4][5]=0
	mat[4][6]=0
	mat[9][9]=7
	mat[9][0]=1
	mat[9][1]=2
	mat[9][2]=3
	mat[9][3]=5
	mat[9][4]=7
	mat[9][5]=8
	mat[9][6]=9
	mat[9][7]=11
	mat[9][8]=12
	mat[9][9]=14
	mat[8][0]=1
	mat[8][1]=2
	mat[8][2]=3
	mat[8][3]=5
	mat[8][4]=7
	mat[8][5]=8
	mat[8][6]=8
	mat[8][7]=11
	mat[8][8]=12
	mat[8][9]=14
	print(mat)
	strpole=""
	for rad in range(10):
		for sl in range(10):
			strpole+=str(mat[rad][sl])+" "
		strpole+="\n"
	print(strpole)
	#print(np.fromstring(ts,dtype=int))
	obr.write( strpole )
