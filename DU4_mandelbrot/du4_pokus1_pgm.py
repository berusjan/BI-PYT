#!/usr/bin/env python3
 
import numpy as np

with open('pokus4.ppm', 'w') as obr:
    # hlavička
	obr.write('P3\n')
	obr.write('10 10\n255\n')
    # data
   # data = '0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0\n 0  3  3  3  3  0  0  7  0  0  7  0  0 11 11 11 11  0  0  0  0  0 15  0\n0  3  0  0  3  0  0  7  0  0  7  0  0 11  0  0 11  0  0  0  0  0 15  0\n0  3  3  3  3  0  0  7  7  7  7  0  0 11  0  0 11  0  0  0  0  0 15  0\n0  3  0  0  3  0  0  7  0  0  7  0  0 11  0  0 11  0  0 15  0  0 15  0\n0  3  0  0  3  0  0  7  0  0  7  0  0 11 11 11 11  0  0 15 15 15 15  0\n0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0\n'
	mat=np.full([10,10,3],0)
	mat[4][7][1]=255
	mat[4][3][1]=255
	mat[4][4][1]=255
	mat[4][5][1]=255
	mat[4][6][1]=255

	mat[4][7][0]=255
	mat[4][3][0]=255
	mat[4][4][0]=255
	mat[4][5][0]=255
	mat[4][6][0]=255

	print(mat)
	strpole=""
	for rad in range(10):
		for sl in range(10):
			for i in range(3):
				strpole+=str(mat[rad][sl][i])+" "
		strpole+="\n"
	print(strpole)
	obr.write( strpole )
