#!/usr/bin/env python3

import random
import os
import time
import sys


if len(sys.argv) !=2:
	print("USAGE:",sys.argv[0],"vystupni_soubor")
	sys.exit()


cols, lines = os.get_terminal_size(0)
print('\033[2J')

FILENAME=sys.argv[1]
MAX_KROK=3
konec=0
x=cols//2
y=lines//2
matrix = [[0 for j in range(cols)] for i in range(lines)]
if cols>lines:
	ciph=len(str(cols))
else:
	ciph=len(str(lines))

if os.path.isfile(FILENAME):
	os.remove(FILENAME)
with open(FILENAME,mode='w',encoding='utf-8') as vystup:

	print('\033[?25l\033[{};{}H\033[40m \033[0m'.format(y,x),end="",flush=True)
	time.sleep(0.05)
	matrix[y][x]+=1
	data=str(y).zfill(ciph)+"\n"+str(x).zfill(ciph)+"\n"+str(1).zfill(2)+"\n \n"
	vystup.write(data)

	while True:
		#pohyb do 8 nahodnych smeru se stejnou pravdepodobnosti (stejny smer libovolne daleko)
		if konec:
			break
		smer=random.randint(1,8)
		krok=random.randint(1,MAX_KROK)
		if smer==1:
			for i in range(krok):
				x=x+1
				y=y+1
				if matrix[y][x]>7:
					pole="o"
					if matrix[y][x]>14:
						pole="\033[1mO"
				else:
					pole=" "
				print('\033[?25l\033[{};{}H\033[{}m{}\033[0m'.format(y,x,40+matrix[y][x]%8,pole),end="",flush=True)
				time.sleep(0.05)
				matrix[y][x]+=1
				data=str(y).zfill(ciph)+"\n"+str(x).zfill(ciph)+"\n"+str(matrix[y][x]).zfill(2)+"\n"+pole+"\n"
				vystup.write(data)
				if x>=cols-1 or x<=0 or y>=lines-1 or y<=0:
					konec=1
					break

		if smer==2:
			for i in range(krok):
				x=x+1	
				y=y-1
				if matrix[y][x]>7:
					pole="o"
					if matrix[y][x]>14:
						pole="\033[1mO"
				else:
					pole=" "
				print('\033[?25l\033[{};{}H\033[{}m{}\033[0m'.format(y,x,40+matrix[y][x]%8,pole),end="",flush=True)
				time.sleep(0.05)
				matrix[y][x]+=1
				data=str(y).zfill(ciph)+"\n"+str(x).zfill(ciph)+"\n"+str(matrix[y][x]).zfill(2)+"\n"+pole+"\n"
				vystup.write(data)
				if x>=cols-1 or x<=0 or y>=lines-1 or y<=0:
					konec=1
					break

		if smer==3:
			for i in range(krok):
				x=x-1
				y=y+1
				if matrix[y][x]>7:
					pole="o"
					if matrix[y][x]>14:
						pole="\033[1mO"
				else:
					pole=" "
				print('\033[?25l\033[{};{}H\033[{}m{}\033[0m'.format(y,x,40+matrix[y][x]%8,pole),end="",flush=True)
				time.sleep(0.05)
				matrix[y][x]+=1
				data=str(y).zfill(ciph)+"\n"+str(x).zfill(ciph)+"\n"+str(matrix[y][x]).zfill(2)+"\n"+pole+"\n"
				vystup.write(data)
				if x>=cols-1 or x<=0 or y>=lines-1 or y<=0:
					konec=1
					break

		if smer==4:
			for i in range(krok):
				x=x-1	
				y=y-1
				if matrix[y][x]>7:
					pole="o"
					if matrix[y][x]>14:
						pole="\033[1mO"
				else:
					pole=" "
				print('\033[?25l\033[{};{}H\033[{}m{}\033[0m'.format(y,x,40+matrix[y][x]%8,pole),end="",flush=True)
				time.sleep(0.05)
				matrix[y][x]+=1
				data=str(y).zfill(ciph)+"\n"+str(x).zfill(ciph)+"\n"+str(matrix[y][x]).zfill(2)+"\n"+pole+"\n"
				vystup.write(data)
				if x>=cols-1 or x<=0 or y>=lines-1 or y<=0:
					konec=1
					break

		if smer==5:
			for i in range(krok):
				y=y+1
				if matrix[y][x]>7:
					pole="o"
					if matrix[y][x]>14:
						pole="\033[1mO"
				else:
					pole=" "
				print('\033[?25l\033[{};{}H\033[{}m{}\033[0m'.format(y,x,40+matrix[y][x]%8,pole),end="",flush=True)
				time.sleep(0.05)
				matrix[y][x]+=1
				data=str(y).zfill(ciph)+"\n"+str(x).zfill(ciph)+"\n"+str(matrix[y][x]).zfill(2)+"\n"+pole+"\n"
				vystup.write(data)
				if x>=cols-1 or x<=0 or y>=lines-1 or y<=0:
					konec=1
					break

		if smer==6:
			for i in range(krok):
				y=y-1
				if matrix[y][x]>7:
					pole="o"
					if matrix[y][x]>14:
						pole="\033[1mO"
				else:
					pole=" "
				print('\033[?25l\033[{};{}H\033[{}m{}\033[0m'.format(y,x,40+matrix[y][x]%8,pole),end="",flush=True)
				time.sleep(0.05)
				matrix[y][x]+=1
				data=str(y).zfill(ciph)+"\n"+str(x).zfill(ciph)+"\n"+str(matrix[y][x]).zfill(2)+"\n"+pole+"\n"
				vystup.write(data)
				if x>=cols-1 or x<=0 or y>=lines-1 or y<=0:
					konec=1
					break

		if smer==7:
			for i in range(krok):
				x=x+1
				if matrix[y][x]>7:
					pole="o"
					if matrix[y][x]>14:
						pole="\033[1mO"
				else:
					pole=" "
				print('\033[?25l\033[{};{}H\033[{}m{}\033[0m'.format(y,x,40+matrix[y][x]%8,pole),end="",flush=True)
				time.sleep(0.05)
				matrix[y][x]+=1
				data=str(y).zfill(ciph)+"\n"+str(x).zfill(ciph)+"\n"+str(matrix[y][x]).zfill(2)+"\n"+pole+"\n"
				vystup.write(data)
				if x>=cols-1 or x<=0 or y>=lines-1 or y<=0:
					konec=1
					break

		if smer==8:
			for i in range(krok):
				x=x-1	
				if matrix[y][x]>7:
					pole="o"
					if matrix[y][x]>14:
						pole="\033[1mO"
				else:
					pole=" "
				print('\033[?25l\033[{};{}H\033[{}m{}\033[0m'.format(y,x,40+matrix[y][x]%8,pole),end="",flush=True)
				time.sleep(0.05)
				matrix[y][x]+=1
				data=str(y).zfill(ciph)+"\n"+str(x).zfill(ciph)+"\n"+str(matrix[y][x]).zfill(2)+"\n"+pole+"\n"
				vystup.write(data)
				if x>=cols-1 or x<=0 or y>=lines-1 or y<=0:
					konec=1
					break
	vystup.close()

print('\033[{};{}H\033[0m\033[?25h'.format(lines,cols))


