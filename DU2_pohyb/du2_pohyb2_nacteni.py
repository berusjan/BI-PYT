#!/usr/bin/env python3

import os
import sys
import time

if len(sys.argv) !=2:
	print("USAGE:",sys.argv[0],"vystupni_soubor_predchoziho_skriptu")
	sys.exit()
FILENAME=sys.argv[1]

if os.path.exists(FILENAME):
	if not os.path.isfile(FILENAME):
		print('Neexstuje soubor {} s vystupem trasy.\nSpustte skript ./du2_pohyb.py {}'.format(FILENAME,FILENAME))
		sys.exit()
else:
	print('Neexstuje soubor \"{}\" s vystupem trasy.\nSpustte skript \"./du2_pohyb.py {}\"'.format(FILENAME,FILENAME))
	sys.exit()

print('\033[2J')

data=[]
with open(FILENAME,encoding='utf-8') as vystup:
	data = vystup.readlines()
	vystup.close()

celkem=len(data)
for bod in range(0,celkem,4):
	text=data[bod]
	y=text[:-1]
	text=data[bod+1]
	x=text[:-1]
	text=data[bod+3]
	pole=text[:-1]
	text=data[bod+2]
	barvatmp=int(text[:-1])
	barva=39+barvatmp%8
	print('\033[?25l\033[{};{}H\033[{}m{}\033[0m'.format(y,x,barva,pole),end="",flush=True)
	time.sleep(0.05)

cols, lines = os.get_terminal_size(0)
print('\033[{};{}H\033[0m\033[?25h'.format(lines,cols))
