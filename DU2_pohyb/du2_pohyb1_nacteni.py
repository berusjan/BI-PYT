#!/usr/bin/env python3

import os
import sys

if len(sys.argv) !=2:
	print("USAGE:",sys.argv[0],"vystupni_soubor_predchoziho_skriptu")
	sys.exit()
FILENAME=sys.argv[1]

if os.path.exists(FILENAME):
	if not os.path.isfile(FILENAME):
		print('Neexstuje soubor {} s vystupem trasy.\nSpustte skript ./du2_pohyb.py {}'.format(FILENAME,FILENAME))
		sys.exit()
else:
	print('Neexstuje soubor \"{}\" s vystupem trasy.\nSpustte skript \"./du2_pohyb.py {}\"'.format(FILENAME,FILENAME))
	sys.exit()


data=[]
with open(FILENAME,encoding='utf-8') as vystup:
	for radek in vystup:
		data.append(radek)
	vystup.close()

cols=(len(data[0])-1)//2
lines=len(data)
matrix = [[0 for j in range(cols)] for i in range(lines)]

j=0
for radek in data:
	i=0
	for sl in range(0,cols*2-1,2):
		string=radek[sl]+radek[sl+1]
		matrix[j][i]=int(string)
		i+=1
	j+=1

print('\033[2J')
for j in range(0,lines):
	for i in range(0,cols):
		if matrix[j][i]:
			if matrix[j][i]>7:
				pole="o"
				if matrix[j][i]>14:
					pole="\033[1mO"
			else:
				pole=" "
			print('\033[?25l\033[{};{}H\033[{}m{}\033[0m'.format(j,i,39+matrix[j][i],pole),end="",flush=True)
		else:
			print('\033[?25l\033[{};{}H \033[0m'.format(j,i),end="",flush=True)
	print("")
print('\033[{};{}H\033[0m\033[?25h'.format(lines,cols))

